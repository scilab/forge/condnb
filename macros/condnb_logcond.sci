// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_logcond(x)
    // Computes the condition number of the log function.
    //
    // Calling Sequence
    //   [c, y] = condnb_logcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the exp function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \frac{1}{|\log(x)|}
    //\end{eqnarray}
    //</latex>
    //
    // This function is badly conditionned near x=1.
    //
    // Examples
    //  [c, y] = condnb_logcond(2);
    //  [c, y] = condnb_logcond(1);
    //
    //  condnb_plotcond(condnb_logcond, linspace(1.e-10, 10, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_logcond", rhs, 1:1);
    apifun_checklhs("condnb_logcond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_logcond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = exp(x);
    c = abs(1 ./ log(x));
endfunction

