// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y, jac] = condnb_sumcond(x)
    // Computes the condition number of the sum function.
    //
    // Calling Sequence
    //   [c, y, jac] = condnb_sumcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //   jac : a 1-by-nx matrix of doubles, the Jacobian of f at point x
    //
    // Description
    // Computes the (relative) condition number of the sum function. 
    // This is with respect to the 1-norm for the input (and with 
    // the absolute value norm for the output).
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \frac { |x_1|+|x_2|+...+|x_n|}{\left| x_1+x_2+...+x_n\right|}
    //\end{eqnarray}
    //</latex>
    //
    // The sum function has a large condition number if
    // the sum is small, but the values are of opposite sign
    // and of very different magnitudes.
    //
    // Examples
    //  // A badly conditionned sum, with inaccurate result
    //  xl = 10^(1:15);
    //  x = [-xl xl+0.1];
    //  [c, y] = condnb_sumcond(x);
    //  expected = 1.5;
    //
    //  // A badly conditionned sum, with accurate result !
    //  // The bad conditionning does not imply that the result is not accurate.
    //  xl = 10^(1:15);
    //  x = [-xl xl];
    //  [c, y] = condnb_sumcond(x);
    //  expected = 0;
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_sumcond", rhs, 1:1);
    apifun_checklhs("condnb_sumcond", lhs, 0:3);

    // Check type
    apifun_checktype("condnb_sumcond", x, "x", 1, "constant");
    // Check size : OK
    apifun_checkveccol("condnb_sumcond", x, "x", 1)
    // Check content : OK

    y = sum(x);
    jac=ones(x)'
    if abs(y) == 0 then
        c = sum(abs(x)) * %inf;
    else
        c = sum(abs(x)) / abs(y);
    end
endfunction

