// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_powcond(x, a)
    // Computes the condition number of the pow function.
    //
    // Calling Sequence
    //   [c, y] = condnb_powcond(x, a)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   a : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the pow function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = |a|
    //\end{eqnarray}
    //</latex>
    //
    // The pow function has a large condition number if a is large.
    //
    // Examples
    //  // Good condition
    //  [c, y] = condnb_powcond(2, 5);
    //
    //  // Extremely bad condition
    //  [c, y] = condnb_powcond(1.000000000000001, 1.e15);
    //  expected = 2.718281828459043876 // With Mathematica
    //
    //  // Another one : http://bugzilla.scilab.org/show_bug.cgi?id=4048
    //  y = 0.9999999999999999^-18014398509482000;
    //  [c, y] = condnb_powcond(0.9999999999999999, -18014398509482000);
    //  expected = 6.05836432903779269; // With XCAS
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_powcond", rhs, 2:2);
    apifun_checklhs("condnb_powcond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_powcond", x, "x", 1, "constant");
    apifun_checktype("condnb_powcond", a, "a", 2, "constant");
    // Check size : OK
    // Check content : OK

    y = x .^ a;
    c = abs(a);
endfunction

