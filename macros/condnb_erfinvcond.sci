// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_erfinvcond(x)
    // Computes the condition number of the erfinv function.
    //
    // Calling Sequence
    //   [c, y] = condnb_erfinvcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the erfinv function.
    //
    // The condition number is :
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \frac{\sqrt{\pi}}{2} \exp(f(x)^2) \left| \frac{x}{f(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // where
    //
    //<latex>
    //\begin{eqnarray}
    //f(x)=\mbox{erfinv}(x).
    //\end{eqnarray}
    //</latex>
    //
    // The erfinv function has a large condition number if
    //   <itemizedlist>
    //   <listitem>x is close to 1,</listitem>
    //   <listitem>x is close to -1.</listitem>
    //   </itemizedlist>
    //
    // Examples
    //  [c, y] = condnb_erfinvcond(0) // 1
    //  [c, y] = condnb_erfinvcond(0.5)
    //  [c, y] = condnb_erfinvcond(1.e2)
    //
    //  condnb_plotcond(condnb_erfinvcond, linspace(-1, 1, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_erfinvcond", rhs, 1:1);
    apifun_checklhs("condnb_erfinvcond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_erfinvcond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = erfinv(x);
    c = zeros(x);
    k = find(x <> 0);
    c(k) =  sqrt(%pi)/2  ..
         .* exp(y(k).^2) ..
         .* abs(x(k))    ..
         ./ abs(y(k));
    k = find(x == 0);
    c(k) = 1;
endfunction

