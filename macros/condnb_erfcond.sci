// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_erfcond(x)
    // Computes the condition number of the erf function.
    //
    // Calling Sequence
    //   [c, y] = condnb_erfcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the erf function.
    //
    // The condition number is :
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \frac{2}{\sqrt{\pi}}
    //\left| \frac{x\exp\left(-x^2\right)}{f(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // where
    //
    //<latex>
    //\begin{eqnarray}
    //f(x) = \mbox{erf}(x)
    //\end{eqnarray}
    //</latex>
    //
    // The erf function is always well conditionned, since its condition
    // number is never greater than 1.
    //
    // Examples
    //  [c, y] = condnb_erfcond(1)
    //  [c, y] = condnb_erfcond(1.e2)
    //  [c, y] = condnb_erfcond(0)
    //
    //  condnb_plotcond(condnb_erfcond, linspace(-6,6,1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_erfcond", rhs, 1:1);
    apifun_checklhs("condnb_erfcond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_erfcond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = erf(x);
    c = zeros(x);
    k = find(x <> 0);
    c(k) =  2/sqrt(%pi)   ..
         .* abs(x(k))     ..
         .* exp(-x(k).^2) ..
         ./ abs(y(k));
    k = find(x == 0);
    c(k) = 1;
endfunction

