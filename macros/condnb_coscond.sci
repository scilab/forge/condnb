// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_coscond(x)
    // Computes the condition number of the cos function.
    //
    // Calling Sequence
    //   [c, y] = condnb_coscond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the cos function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \left| \frac{x \sin(x)}{\cos(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // The cos function has a large condition number if
    //   <itemizedlist>
    //   <listitem>x is large,</listitem>
    //   <listitem>
    //       x=k*pi/2, where k is an odd integer (where cos(x)=0).
    //   </listitem>
    //   </itemizedlist>
    //
    // Examples
    //  [c, y] = condnb_coscond(1)     // ~1
    //  [c, y] = condnb_coscond(%pi/2) // ~10^16
    //
    //  condnb_plotcond(condnb_coscond, linspace(-4, 4, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_coscond", rhs, 1:1);
    apifun_checklhs("condnb_coscond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_coscond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = cos(x);
    c = abs(x.*sin(x)) ./ abs(y);
endfunction

