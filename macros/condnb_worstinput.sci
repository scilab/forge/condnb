// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, c]=condnb_worstinput(varargin)
    // Computes the input producing the greatest condition number
    //
    // Calling Sequence
    //   x = condnb_worstinput(condfunc, bounds)
    //   x = condnb_worstinput(condfunc, bounds, nbrestarts)
    //   [x, c] = condnb_worstinput(...)
    //
    // Parameters
    //   condfunc : a function, the condition number function
    //   bounds : a nx-by-2 matrix of doubles, the bounds of the input. The first column contains the minmimums, the second column contains the maximums.
    //   nbrestarts : a double, integer value, greater or equal to one, the number of restarts of the algorithm (default = 5)
    //   x : a nx-by-1 matrix of doubles, the input with greatest condition number
    //   c : a ny-by-1 matrix of doubles, the greatest condition number
    //
    // Description
    //   Solves the following optimization problem :
    //
    //<latex>
    //\begin{eqnarray}
    //x = \textrm{argmax}_{x\in B} \; c(x)
    //\end{eqnarray}
    //</latex>
    //
    // where c is the condition number of the function f, 
    // and B is the interval given by the <literal>bounds</literal> 
    // variable.
    //
    // The condtion function condfunc must have the following 
    // calling sequence :
    //
    //<programlisting>
    // c = condfunc(x)
    //</programlisting>
    //
    // where c is the condition number (a double).
    //
    // The algorithm uses a multi-start method, with an initial 
    // point x0 which is a random vector uniform in the bounds. 
    // For each x0, the algorithm performs a local search with 
    // the BFGS method in the optim function. 
    //
    // Examples
    // // Search for ill-conditionned data for prod function    
    // condfunc=condnb_prodcond;
    // n=2; // Dimension of the problem
    // binf = -1*ones(n,1);
    // bsup = 1*ones(n,1);
    // bounds=[binf,bsup];
    // [xbest,cbest]=condnb_worstinput(condfunc,bounds)
    //
    // // Search for ill-conditionned data for sin function
    // condfunc=condnb_sincond;
    // bounds = [-10,10];
    // [xbest,cbest]=condnb_worstinput(condfunc,bounds)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_worstinput", rhs, 2:3);
    apifun_checklhs("condnb_worstinput", lhs, 0:2);

    // Get arguments
    condfunc = varargin(1);
    bounds = varargin(2);
    nbrestarts = apifun_argindefault(varargin, 3, 5);

    // Check types
    apifun_checktype("condnb_worstinput", condfunc, "condfunc", 1, ["function","list"]);
    apifun_checktype("condnb_worstinput", bounds, "bounds", 2, "constant");
    apifun_checktype("condnb_worstinput", nbrestarts, "nbrestarts", 3, "constant");

    // Check size
    // condfunc : nothing to check
    nx=size(bounds,"r")
    apifun_checkdims("condnb_worstinput", bounds, "bounds", 2, [nx,2]);
    apifun_checkscalar("condnb_worstinput", nbrestarts, "nbrestarts", 3);

    // Check content
    apifun_checkgreq("condnb_worstinput", nbrestarts, "nbrestarts", 3, 1);

    function [f, g, ind]=condnb_optimcost(x, ind, condfunc)
        c = condfunc(x)
        f=-c
        g = - numderivative(condfunc,x)
    endfunction
    // The algorithm minimizes the opposite of the condition number
    x=%inf
    c=%inf
    binf = bounds(:,1)
    bsup = bounds(:,2)
    minimumcond=-1/(%eps*10)
    for i=1:nbrestarts
        x0 = distfun_unifrnd(binf,bsup,nx,1);
        [copt, xopt] = optim(list(condnb_optimcost,condfunc), "b",binf,bsup, x0);
        if (copt<c) then
            x=xopt;
            c=copt;
        end
        if (c<minimumcond) then
            // The point is sufficiently bad : stop
            break
        end
    end
    // The output is the condition number
    c=-c
endfunction
