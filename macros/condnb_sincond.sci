// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y, jac] = condnb_sincond(x)
    // Computes the condition number of the sin function.
    //
    // Calling Sequence
    //   [c, y, jac] = condnb_sincond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //   jac : a n-by-m, matrix of doubles, the function derivatives
    //
    // Description
    // Computes the condition number of the sin function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \left| \frac{x \cos(x)}{\sin(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // The sin function has a large condition number if
    //   <itemizedlist>
    //   <listitem>x is large,</listitem>
    //   <listitem>
    //       x is a nonzero integer multiple of pi(where sin(x) = 0).
    //   </listitem>
    //   </itemizedlist>
    //
    // Examples
    //  [c, y] = condnb_sincond(1)
    //  c = condnb_sincond(0); // c = 1
    //
    //  // http://bugzilla.scilab.org/show_bug.cgi?id=5738
    //  [c, y] = condnb_sincond(%pi);
    //  245850922/78256779 == %pi;
    //  // XCAS : evalf(sin(245850922/78256779), 50);
    //  expected = 0.781793661990754354001e-16;
    //
    //  condnb_plotcond(condnb_sincond, linspace(-4, 4, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_sincond", rhs, 1:1);
    apifun_checklhs("condnb_sincond", lhs, 0:3);

    // Check type
    apifun_checktype("condnb_sincond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = sin(x);
    jac = cos(x)
    c = zeros(x);
    k = find(x <> 0);
    c(k) =  abs(x(k) .* jac(k))./ abs(y(k));
    k = find(x == 0);
    c(k) = 1;
endfunction

