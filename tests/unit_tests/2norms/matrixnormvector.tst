// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

function checkVectorProperty(A,normflag)
    x=condnb_matrixnormvector(A,normflag);
    nA=condnb_matrixnorm(A,normflag);
    p=size(x,"c");
    for j=1:p
        r=norm(A*x(:,j),normflag)/norm(x(:,j),normflag);
        assert_checkalmostequal(r,nA);
    end
endfunction

/////////////////
// 
// 1-norm
//
A = [1 1];
x=condnb_matrixnormvector(A,1);
assert_checkequal ( x , eye(2,2));
checkVectorProperty(A,1)
//
// rank-2 matrix
A = [1 2;3 4];
x=condnb_matrixnormvector(A,1);
assert_checkequal ( x , [0;1]);
checkVectorProperty(A,1)
//
// A matrix with negative entries
A = [3 -4;-3 4];
x=condnb_matrixnormvector(A,1);
assert_checkequal ( x , [0;1]);
checkVectorProperty(A,1)
/////////////////
// 
// INF-norm
//
A = [1 1];
x=condnb_matrixnormvector(A,%inf);
assert_checkequal ( x , [1;1]);
checkVectorProperty(A,%inf)
//
// rank-2 matrix
A = [1 2;3 4];
x=condnb_matrixnormvector(A,%inf);
assert_checkequal ( x , [1;1]);
checkVectorProperty(A,%inf)
//
// A matrix with negative entries
A = [3 -4;-3 4];
x=condnb_matrixnormvector(A,%inf);
assert_checkequal ( x , [1,-1;-1,1]);
checkVectorProperty(A,%inf)
/////////////////
// 
// 2-norm
//
A = [1 1];
x=condnb_matrixnormvector(A,2);
assert_checkalmostequal ( x , [0.7071068;0.7071068],1.e-6);
checkVectorProperty(A,2)
//
// rank-2 matrix
A = [1 2;3 4];
x=condnb_matrixnormvector(A,2);
assert_checkalmostequal ( x , [0.5760484;0.8174156],1.e-6);
checkVectorProperty(A,2)
