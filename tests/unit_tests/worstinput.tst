// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- ENGLISH IMPOSED -->
// <-- CLI SHELL MODE -->


// Search for ill-conditionned data for sum function
n=2; // Dimension of the problem
binf = -1*ones(n,1);
bsup = 1*ones(n,1);
bounds=[binf,bsup];
[xbest,cbest]=condnb_worstinput(condnb_sumcond,bounds);
assert_checkequal(sum(xbest),0);
assert_checktrue(cbest>1.e13);

// Search for ill-conditionned data for prod function
n=2; // Dimension of the problem
binf = -1*ones(n,1);
bsup = 1*ones(n,1);
bounds=[binf,bsup];
[xbest,cbest]=condnb_worstinput(condnb_prodcond,bounds);
assert_checkalmostequal(prod(xbest),0,[],1.e-7);
assert_checktrue(cbest>1.e13);

// Search for ill-conditionned data for sin function
bounds = [-10,10];
[xbest,cbest]=condnb_worstinput(condnb_sincond,bounds);
assert_checkalmostequal(abs(sin(xbest)),0,[],1.e-5);
assert_checktrue(cbest>1.e9);
