// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- ENGLISH IMPOSED -->
// <-- CLI SHELL MODE -->


x =[-1;1.e-6;1.];
delta= [0.;1.e-6;0];
normflag=2;
rp = condnb_perturbation(condnb_sumcond, x, delta, normflag);
assert_checkalmostequal(rp,1.33355353e+12);

// Compute the perturbation from the jacobian
// This gives the worst possible increment.
x =[-1;1.e-6;1.];
epsilon=1.e-6;
normflag=2;
[c,y,jac]=condnb_sumcond(x);
delta=epsilon*condnb_matrixnormvector(jac,normflag);
rp = condnb_perturbation(condnb_sumcond, x, delta, normflag);
assert_checkalmostequal(rp,3.21618691e+12);

// Compute the perturbation from the jacobian
// This gives the worst possible increment.
x =[-1;1.e-6;1.];
epsilon=1.e-6;
normflag=2;
[c,y,jac]=condnb_sumcond(x);
delta=epsilon*condnb_matrixnormvector(jac,normflag);
rp = condnb_perturbation(condnb_sumcond, x, delta, normflag);
assert_checkalmostequal(rp,3.21618691e+12);

// Check with several deltas
x =[-1;1.e-6;1.];
delta= [
1.e-6 0.    0 
0     1.e-6 0
0     0     1.e-6
];
normflag=2;
rp = condnb_perturbation(condnb_sumcond, x, delta, normflag);
assert_checkalmostequal(rp(1),2.00000050e+06);
assert_checkalmostequal(rp(2),1.33355353e+12);
assert_checkalmostequal(rp(3),1.99999950e+06);

// Compute the perturbation from the jacobian
// This gives the worst possible increment.
x =[-1;0;1.];
normflag=2;
delta= [0.;1.e-6;0];
rp = condnb_perturbation(condnb_sumcond, x, delta, normflag);
assert_checkequal(rp,%inf);
