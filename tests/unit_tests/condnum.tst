// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

function [c,y] = sincond (x)
    // Condition number of the sin function
    y = sin(x)
    c = zeros(x)
    k = find(x<>0)
    c(k) = abs(x(k).*cos(x(k))) ./ abs(y(k))
    k = find(x==0)
    c(k) = 1
endfunction

function [c,y] = sumcond(x)
    // Condition number of the sum function
    y = sum(x)
    if ( abs(y) == 0 ) then
        c = %inf
    else
        c = sum(abs(x)) / abs(y)
    end
endfunction

function [c,y] = sqrtcond(x)
    // Condition number of the sqrt function
    y = sqrt(x)
    c = 1/2
endfunction

x = 1.e-100;
[cc,yc] = condnb_condnum ( sin , x );
[ce,ye] = sincond ( x );
assert_checkalmostequal ( cc , ce , 1.e-11 );
assert_checkequal ( yc , ye );
//
x = 0;
[cc,yc] = condnb_condnum ( sin , x );
//[ce,ye] = sincond ( x );
// The condnum function cannot remove the singularity as sincond does.
assert_checkequal ( isnan(cc) , %t ); 
assert_checkequal ( yc , 0 );
//
x = 3.14159
[cc,yc] = condnb_condnum ( sin , x );
[ce,ye] = sincond ( x );
assert_checkalmostequal ( cc , ce , 1.e-10 );
assert_checkequal ( yc , ye );
//
x = 3.141592653;
[cc,yc] = condnb_condnum ( sin , x );
[ce,ye] = sincond ( x );
assert_checkalmostequal ( cc , ce , 1.e-10 );
assert_checkequal ( yc , ye );
//
x = 3.1415926535898;
[cc,yc] = condnb_condnum ( sin , x );
[ce,ye] = sincond ( x );
assert_checkalmostequal ( cc , ce , 1.e-10 );
assert_checkequal ( yc , ye );
//
x = %pi;
[cc,yc] = condnb_condnum ( sin , x );
[ce,ye] = sincond ( x );
assert_checkalmostequal ( cc , ce , 1.e-10 );
assert_checkequal ( yc , ye );
//
// Use a large x
x = 1.000000357564167061e5;
// Avoid the scaling for x in numderivative : force h
h=%eps^(1/5);
[cc,yc] = condnb_condnum ( sin , x , 4 , %eps^(1/5));
[ce,ye] = sincond ( x );
assert_checkalmostequal ( cc , ce , 1.e-6 );
assert_checkequal ( yc , ye );
//

//
// An ill-conditionned case for the sum.
xl = (10^(1:15))';
x = [-xl;xl+0.1];
[cc,yc] = condnb_condnum ( sum , x );
//[ce,ye] = sumcond(x); // ce = 1.269841270D+15
assert_checkequal ( floor(log10(cc)) , 15 );
assert_checkalmostequal ( yc , 1.5 , 1.e-0 );
//
x = 1.;
[cc,yc] = condnb_condnum ( sqrt , x );
[ce,ye] = sqrtcond ( x );
assert_checkalmostequal ( cc , ce , 1.e-11 );
assert_checkequal ( yc , ye );
//
// Check various orders
x = 1.;
precision = [1.e-7 1.e-11 1.e-12 1.e-13];
order = [1 2 4];
[ce,ye] = sqrtcond ( x );
for k = 1:3
    [cc,yc] = condnb_condnum ( sqrt , x , order(k) );
    assert_checkalmostequal ( cc , ce , precision(k) );
    assert_checkequal ( yc , ye );
end
//
// Use default order, but configure h
[cc,yc] = condnb_condnum ( sqrt , 1 , [] , 1.e-8 );
[ce,ye] = sqrtcond ( x );
assert_checkalmostequal ( cc , ce , 1.e-8 );
assert_checkequal ( yc , ye );
//
// Use default order and h
[cc,yc] = condnb_condnum ( sqrt , 1 , [] , [] );
[ce,ye] = sqrtcond ( x );
assert_checkalmostequal ( cc , ce , 1.e-11 );
assert_checkequal ( yc , ye );
//
// Set order, use default h
[cc,yc] = condnb_condnum ( sqrt , 1 , 4 , [] );
[ce,ye] = sqrtcond ( x );
assert_checkalmostequal ( cc , ce , 1.e-12 );
assert_checkequal ( yc , ye );
//
// Check various steps
x = 1.;
steps     = [1.e-4 1.e-6  1.e-8 1.e-10];
precision = [1.e-8 1.e-10 1.e-8 1.e-7];
[ce,ye] = sqrtcond ( x );
for k = 1:4
    [cc,yc] = condnb_condnum ( sqrt , x , [] , steps(k) );
    assert_checkalmostequal ( cc , ce , precision(k) );
    assert_checkequal ( yc , ye );
end
//
function y = myfun ( x , a , b , c )
    y = sin(a*x^2+b*x+c)
endfunction
function [c,y] = myfuncond(x,a,b,c)
    // Condition number of the myfun function
    y = myfun ( x , a , b , c )
    fp = cos(a*x^2+b*x+c) * (2*a*x+b)
    c = abs(x*fp)/abs(y)
endfunction
x = 1;
f = list(myfun,1,2,3);
order = [1 2 4];
ye = myfun(x,1,2,3);
[ce,ye] = myfuncond(x,1,2,3);
precision = [1.e-7 1.e-10 1.e-12 1.e-11];
for k = 1 : 3
    [cc,yc] = condnb_condnum ( f , x , order(k) );
    assert_checkalmostequal ( cc , ce , precision(k) );
    assert_checkequal ( yc , ye );
end

