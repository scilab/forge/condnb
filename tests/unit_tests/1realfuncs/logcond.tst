// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[c,y] = condnb_logcond ( 2 );
assert_checkalmostequal ( y , 7.389056098930649519 , 10*%eps );
assert_checkalmostequal ( c , 1.442695040888963387 , 10*%eps );
//
ieee(2);
[c,y] = condnb_logcond ( 1 );
assert_checkalmostequal ( y , 2.718281828459045091 , 10*%eps );
assert_checkequal ( c , %inf );


