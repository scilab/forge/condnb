// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[c,y] = condnb_tancond ( 1 );
assert_checkalmostequal ( y , 1.5574077247 , 1.e-8 );
assert_checkalmostequal ( c , 2.1995003406 , 1.e-8 );
//
[c,y] = condnb_tancond ( %pi );
assert_checkalmostequal ( y , -7.81793661990754354001e-17 , 1 );
assert_checkalmostequal ( c , 2.5653050788D+16 , 1.e-8 );
//
[c,y] = condnb_tancond ( 0 );
assert_checkequal ( y , 0 );
assert_checkequal ( c , 1 );

