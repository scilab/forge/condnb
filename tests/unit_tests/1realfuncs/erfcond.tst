// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[c,y] = condnb_erfcond ( 1 );
assert_checkalmostequal ( y , 0.842700792949715 , 10*%eps );
assert_checkalmostequal ( c , 0.492591796392631 , 10*%eps );
//
[c,y] = condnb_erfcond ( 0 );
assert_checkequal ( y , 0 );
assert_checkequal ( c , 1 );
//
c = condnb_erfcond ( [1 0] );
assert_checkalmostequal ( c , [0.492591796392631 1], 10*%eps );

