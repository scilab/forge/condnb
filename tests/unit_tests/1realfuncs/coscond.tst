// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[c,y] = condnb_coscond ( 1 );
assert_checkalmostequal ( y , 5.4030230587D-01 , 1.e-8 );
assert_checkalmostequal ( c , 1.5574077247D+00 , 1.e-8 );
//
[c,y] = condnb_coscond ( %pi/2 );
assert_checkalmostequal ( y , 3.908968309953771770e-17 , 1 );
assert_checkalmostequal ( c , 2.5653050788D+16 , 1.e-1 );

