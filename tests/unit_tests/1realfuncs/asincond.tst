// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// condnb_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_checkalmostequal ( computed, expected, epsilon )
  if (expected==0.0) then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

function flag = assert_checkequal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

[c,y] = condnb_asincond ( 0.5 );
assert_checkalmostequal ( y , 0.523598775598299 , %eps );
assert_checkalmostequal ( c , 1.10265779084358 , 100*%eps );
//
[c,y] = condnb_asincond ( 0 );
assert_checkequal ( y , 0 );
assert_checkequal ( c , 1 );
//
[c,y] = condnb_asincond ( 1 );
assert_checkequal ( y , %pi/2 );
assert_checkequal ( c , %inf );
//
[c,y] = condnb_asincond ( -1 );
assert_checkequal ( y , -%pi/2 );
assert_checkequal ( c , %inf );

