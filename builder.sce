// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function condnbBuildToolbox()
    //setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES")

    TOOLBOX_NAME  = "condnb";
    TOOLBOX_TITLE = "Condnb";

    toolbox_dir = get_absolute_file_path("builder.sce");

    mode(-1);
    lines(0);
    try
        getversion("scilab");
    catch
        error(gettext("Scilab 5.0 or more is required."));  
    end;
    // ====================================================================
    if ~with_module("development_tools") then
        error(msprintf(gettext("%s module not installed."),"development_tools"));
    end

    tbx_builder_macros(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction

condnbBuildToolbox();
clear condnbBuildToolbox;
