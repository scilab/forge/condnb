// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// Search for ill-conditionned data in a given interval

function searchillconditionned()

    // 1. Search for ill-conditionned data for sin function
    mprintf("Search for ill conditionned data for sin function.\n");
    condfunc=condnb_sincond
    bounds=[-10,10]
    [xbest,cbest]=condnb_worstinput(condfunc,bounds)
    mprintf("xbest=%s, cbest=%s\n", string(xbest), string(cbest))
    [c,y]=condfunc(xbest)
    subplot(2,1,1)
    plot(xbest,y,"r*")
    legend("Worst")
    subplot(2,1,2)
    plot(xbest,cbest,"r*")
    legend("Worst")
    condnb_plotcond(condfunc, linspace(bounds(1), bounds(2), 1000));

endfunction

searchillconditionned();
clear searchillconditionned
