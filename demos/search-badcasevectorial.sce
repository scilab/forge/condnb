// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// Search for ill-conditionned data in a given interval

function searchillconditionned()

    // 1. Search for ill-conditionned data for sum function
    mprintf("1. Search for ill-conditionned data for sum function\n")
    n=5; // Dimension of the problem
    binf = -1*ones(n,1);
    bsup = 1*ones(n,1);
    bounds=[binf,bsup];
    [xbest,cbest]=condnb_worstinput(condnb_sumcond,bounds);
    mprintf("cbest=%e\n", cbest)
    mprintf("xbest =")
    mprintf("%.2e ",xbest)
    mprintf("\n")

    // 2. Search for ill-conditionned data for prod function
    mprintf("\n")
    mprintf("2. Search for ill-conditionned data for prod function\n")
    n=5; // Dimension of the problem
    binf = -1*ones(n,1);
    bsup = 1*ones(n,1);
    bounds=[binf,bsup];
    [xbest,cbest]=condnb_worstinput(condnb_prodcond,bounds);
    mprintf("cbest=%e\n", cbest)
    mprintf("xbest =")
    mprintf("%.2e ",xbest)
    mprintf("\n")

    // Compute perturbation for the sum
    mprintf("\n")
    mprintf("3. Compute perturbation for the sum\n")
    x=[1.e-6;-1.;-1.;1.;1.]
    mprintf("x =")
    mprintf("%.2e ",x)
    mprintf("\n")
    [c,y,jac]=condnb_sumcond(x)
    normflag=2
    epsilon=1.e-6
    mprintf("Value: %s\n", string(y))
    mprintf("Condition number: %e\n", c)
    // Compute worst perturbation
    mprintf("Epsilon=%f\n",epsilon)
    mprintf("Norm: %d\n", normflag)
    delta=epsilon*condnb_matrixnormvector(jac,normflag)
    rp = condnb_perturbation(condnb_sumcond, x, delta, normflag)
    mprintf("Delta=\n")
    mprintf("")
    mprintf("%.2e ",delta)
    mprintf("\n")
    mprintf("Relative perturbation=%e\n",rp)
endfunction

searchillconditionned();
clear searchillconditionned
