// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Main
mprintf("Updating condnb : main functions\n");
helpdir = cwd;
funmat = [
  "condnb_condnum"
  "condnb_plotcond"
  "condnb_worstinput"
  "condnb_perturbation"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "condnb";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


//
// Scalar functions
mprintf("Updating condnb : Scalar functions\n");
helpdir = fullfile(cwd,"1realfuncs");
funmat = [
  "condnb_asincond"
  "condnb_acoscond"
  "condnb_atancond"
  "condnb_sincond"
  "condnb_coscond"
  "condnb_erfcond"
  "condnb_erfinvcond"
  "condnb_tancond"
  "condnb_sqrtcond"
  "condnb_expcond"
  "condnb_logcond"
  "condnb_powcond"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "condnb";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


//
// Multivariate functions
mprintf("Updating condnb : Multivariate functions\n");
helpdir = fullfile(cwd,"3multifuncs");
funmat = [
  "condnb_sumcond"
  "condnb_prodcond"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "condnb";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


//
// Norms
mprintf("Updating condnb : norms\n");
helpdir = fullfile(cwd,"2norms");
funmat = [
  "condnb_matrixnorm"
  "condnb_matrixnormvector"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "condnb";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


