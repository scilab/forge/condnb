<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from condnb_matrixnormvector.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="condnb_matrixnormvector" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>condnb_matrixnormvector</refname>
    <refpurpose>Computes the vector maximizing a given matrix norm</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x = condnb_matrixnormvector(A)
   x = condnb_matrixnormvector(A,normflag)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A :</term>
      <listitem><para> a m-by-n matrix of doubles, the matrix</para></listitem></varlistentry>
   <varlistentry><term>normflag :</term>
      <listitem><para> a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a n-by-p matrix of doubles, the vectors</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the vector x which achieves the matrix given matrix norm.
   </para>
   <para>
This vector is so that
   </para>
   <para>
<latex>
\begin{eqnarray}
\frac{\|Ax\|}{\|x\|} = \|A\|
\end{eqnarray}
</latex>
   </para>
   <para>
in the sense of the given norm.
By definition of the matrix norm, the right-hand side
of the previous equation is the norm of the matrix A.
   </para>
   <para>
It might happen that several (say p) vectors x have this property.
In this case, the p columns in x collect all such vectors.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A = [1 1]
x = condnb_matrixnormvector(A,1)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2016 - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Numerical Linear Algebra", Allaire, Kaber, Springer (2007), Chapter 3, Proposition 3.1.2, p.48</para>
</refsection>
</refentry>
