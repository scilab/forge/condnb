<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from condnb_matrixnorm.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="condnb_matrixnorm" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>condnb_matrixnorm</refname>
    <refpurpose>Computes the matrix norm</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   n = condnb_matrixnorm(A)
   n = condnb_matrixnorm(A, normflag)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A :</term>
      <listitem><para> a m-by-n matrix of doubles, the matrix</para></listitem></varlistentry>
   <varlistentry><term>normflag :</term>
      <listitem><para> a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a double, the matrix norm</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the matrix norm of A.
   </para>
   <para>
The 1-norm of A is :
   </para>
   <para>
<latex>
\begin{eqnarray}
\|A\|_1 = \max_{j=1,n} \sum_{i=1}^m |a_{i,j}|
\end{eqnarray}
</latex>
   </para>
   <para>
The infinity-norm of A is :
   </para>
   <para>
<latex>
\begin{eqnarray}
\|A\|_\infty = \max_{i=1,m} \sum_{j=1}^n |a_{i,j}|
\end{eqnarray}
</latex>
   </para>
   <para>
The 2-norm of A is :
   </para>
   <para>
<latex>
\begin{eqnarray}
\|A\|_2 = \max_{i=1,r} \sigma_i
\end{eqnarray}
</latex>
   </para>
   <para>
where <latex>\sigma_i</latex> are the singular values of
A and r is the rank of A.
   </para>
   <para>
The goal of this function is to workaround a bug
in Scilab's "norm" function.
Indeed, in the case where A is a 1-by-n row matrix,
Scilab's "norm" uses a vector norm instead of the expected
matrix norm.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A=[1 1]
condnb_matrixnorm(A)      // 1-norm = 1
condnb_matrixnorm(A,1)    // 1-norm = 1
condnb_matrixnorm(A,%inf) // INF-norm = 2
condnb_matrixnorm(A,2)    // 2-norm = sqrt(2)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2016 - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Numerical Linear Algebra", Allaire, Kaber, Springer (2007), Chapter 3, Proposition 3.1.2, p.48</para>
</refsection>
</refentry>
