<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from condnb_plotcond.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="condnb_plotcond" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>condnb_plotcond</refname>
    <refpurpose>Plots the condition number of a function.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   condnb_plotcond(f, x)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>f :</term>
      <listitem><para> a function or a list, the function to plot</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a m-by-1 or 1-by-m matrix of doubles, the x values.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Plots the condition number of the function with the function itself.
Automatically switches to log-scale for f and its condition, if possible
and if necessary.
Move the y-label to the left if x data contains mixed signs.
   </para>
   <para>
The header of the function f must be
<programlisting>
[c, y] = f(x)
</programlisting>
where x is the current point,
y is the function value and
c is the condition number.
   </para>
   <para>
It might happen that the function requires additionnal
arguments to be evaluated.
In this case, we can use the following feature.
The function f can also be the list(fun, a1, a2, ...).
In this case fun, the first element in the list, must have the header:
<programlisting>
[c, y] = fun(x, a1, a2, ...)
</programlisting>
where the input arguments a1, a2, ...
will be automatically be appended at the end of the calling sequence.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// The values of atan and its condition are regular:
// use a regular scale.
scf();
condnb_plotcond(condnb_atancond, linspace(-1, 1, 1000));

// The condition values of acos are positive and of
// very different magnitudes:
// automatically switches to log scale.
scf();
condnb_plotcond(condnb_acoscond, linspace(-1, 1, 1000));
scf();
condnb_plotcond(condnb_sincond, linspace(-4, 4, 1000));

// The exp values are positive and of very different magnitudes:
// automatically switches to log scale.
scf();
condnb_plotcond(condnb_expcond, linspace(-7e2, 7.e2, 1000));

// The values of the condition number are very close to zero,
// but we keep the regular scale, as low condition numbers
// are of no interest for us.
scf();
condnb_plotcond(condnb_erfcond, linspace(-6, 6, 1000));

// A case where we plot a customized condition number function.
function x = myinvnorstd(p)
mu = zeros(p);
std = ones(p);
q = 1 - p;
x = cdfnor("X", mu, std, p, q);
endfunction
function [c, x] = myinvnorstdcond(p)
for k = 1 : size(p, "*")
[ck, xk] = condnb_condnum(myinvnorstd, p(k));
c(k) = ck;
x(k) = xk;
end
endfunction
h = scf();
condnb_plotcond(myinvnorstdcond, linspace(1e-5, 1-1e-5, 1000));

// A case where we plot a customized condition number function,
// which needs additionnal arguments.
function x = myinvnor(p, mu, std)
q = 1 - p;
x = cdfnor("X", ones(p)*mu, ones(p)*std, p, q);
endfunction
function [c, x] = myinvnorcond(p, mu, std)
for k = 1 : size(p, "*")
[ck, xk] = condnb_condnum(list(myinvnor, mu, std), p(k));
c(k) = ck;
x(k) = xk;
end
endfunction
h = scf();
condnb_plotcond(list(myinvnorcond, 1, 2), linspace(1e-5, 1-1e-5, 1000));

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, DIGITEO, 2010</member>
   </simplelist>
</refsection>
</refentry>
